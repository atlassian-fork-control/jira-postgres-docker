FROM postgres:9.5
COPY ./docker-entrypoint-initdb.d/* /docker-entrypoint-initdb.d
ENV POSTGRES_USER jira
ENV POSTGRES_PASSWORD jira
